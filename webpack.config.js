const path = require('path');

module.exports = {
	entry: ['./resources/js/app.js'],
	watch: true,
	mode: 'development',
	watchOptions: {
		aggregateTimeout: 300,
		poll: 1000,
		ignored: 'node_modules'
	},
	resolve: {
		extensions: ['*', '.js', '.jsx']
	},
	output: {
		filename: 'app.bundle.js',
		path: path.resolve(__dirname, 'public/js')
	},
	module: {
		rules: [
		    {
		        test: /\.(js|jsx)$/,
		        exclude: /node_modules/,
		        use: ['babel-loader']
		    },
			{
				test: /\.scss$/,
				use: [
	                "style-loader", //outputs our CSS into a <style> tag in the document.
	                "css-loader", //parses the CSS into JavaScript and resolves any dependencies.
	                "sass-loader" //transforms Sass into CSS.
				]
			}
		]
	},
};