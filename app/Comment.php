<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $fillable = [
        'user_id','user_name','game_id','response_to','content',
    ];

    public function responses(){
    	return $this->hasMany(Comment::class, 'response_to', 'id')->with('responses');
    }
    
}
