<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function getUser($id)
    {
        return User::with(['games'])->where('user_id', $id)->get();
    }
}
