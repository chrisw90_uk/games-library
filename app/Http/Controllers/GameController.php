<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
    public function getGames()
    {
        $user = Auth::user();

        return Game::orderBy('name', 'asc')->where('user_id', $user->id)->get();
    }

    public function getGame($id)
    {
        $user = Auth::user();

        return Game::with(['comments' => function($query) { $query->where('response_to', null); }, 'comments.responses'])->where('user_id', $user->id)->where('id', $id)->firstOrFail();
    }

    public function getUserGame(Request $request)
    {
        return Game::with(['comments' => function($query) { $query->where('response_to', null); }, 'comments.responses'])->where('user_id', $request->user_id)->where('id', $id)->firstOrFail();
    }

    public function saveGame(Request $request)
    {
        $user = Auth::user();

        $game = $user->games()->create([
            'name' => $request->name,
            'publisher' => $request->publisher,
            'genre' => $request->genre
        ]);

        return ['status' => 'Entry saved!'];
    }
}
