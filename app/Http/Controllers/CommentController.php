<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function addComment(Request $request)
    {
        $user = Auth::user();

        $data = Comment::create([
            'user_id' => $user->id,
            'user_name' => $user->name,
            'game_id' => $request->game_id,
            'content' => $request->content
        ]);

        return array('id' => $data->id, 'user_name'=>$user->name);
    }

    public function addResponse(Request $request)
    {
        $user = Auth::user();

        $data = Comment::create([
            'user_id' => $user->id,
            'user_name' => $user->name,
            'game_id' => $request->game_id,
            'response_to' => $request->response_to,
            'content' => $request->content
        ]);

        return array('id' => $data->id, 'user_name'=>$user->name);
    }
}
