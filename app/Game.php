<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{
	use SoftDeletes;
    protected $fillable = [
        'image_url','name','publisher','genre'
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
