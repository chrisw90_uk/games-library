<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');
Route::get('/dashboard/{any}', 'HomeController@index')->where('any', '.*');


//Users

Route::get('api/users', 'UserController@getUsers');
Route::get('api/user', 'UserController@getUser');

//Games

Route::get('api/games', 'GameController@getGames');
Route::get('api/user/game/{id}', 'GameController@getUserGame');
Route::get('api/game/{id}', 'GameController@getGame');
Route::post('api/game', 'GameController@saveGame');

//Comment

Route::post('api/comment', 'CommentController@addComment');

Route::post('api/response', 'CommentController@addResponse');
