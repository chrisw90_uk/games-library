import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

export class GamesView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            working: true,
            data: {},
            comment: '',
            response: '',
            commenting: false
        };
        this.addComment = this.addComment.bind(this);
        this.toggleResponse = this.toggleResponse.bind(this);
        this.addResponse = this.addResponse.bind(this);
        this.handleComment = this.handleComment.bind(this);
        this.renderResponses = this.renderResponses.bind(this);
    }
    componentDidMount() {
      this.getGame()
    }
    getGame(){
      this.setState({working: true})
      axios.get('/api/game/' + this.props.match.params.id).then(res => {      
        this.setState({data: res.data, working: false})
      })
    }
    addComment(e) {
      e.preventDefault();
      this.setState({commenting: true})
      let comment = {
        game_id: this.props.match.params.id,
        content: this.state.comment
      }
      axios.post('/api/comment', comment).then(res => {
          comment.id = res.data.id
          comment.user_name = res.data.user_name
          comment.responses = [];
          comment.commenting = false;
          this.state.data.comments.push(comment)

          this.setState({data: this.state.data, comment: '', commenting: false})
      })
    }
    toggleResponse(comment){
        comment.responding = true;
        console.log(comment);
        this.setState({data: this.state.data})
    }
    addResponse(comment, e){
        e.preventDefault();
        
        let response = {
            response_to: comment.id,
            game_id: this.props.match.params.id,
            content: this.state.response
          }
        axios.post('/api/response', response).then(res => {
            response.id = res.data.id
            response.user_name = res.data.user_name
            response.responses = [];
            comment.responding = false;
           comment.responses.push(response)
           this.setState({data: this.state.data, response: '', commenting: false})
      })
    }
    handleComment(e){
      this.setState({ [e.target.name]: e.target.value });
    }
    renderResponses(responses){
        return(
            <ul>
                {responses.map(r =>

                    <li key={r.id} className="game__comments-item">
                        <div className="game__comments-item-inner">
                            <label><em>{r.user_name}</em></label>
                            <span>{r.content}</span>
                            {r.responding ? (
                                <form className="game__comments-add" onSubmit={(e) => this.addResponse(r, e)}>
                                <h5>Add response</h5>
                                <textarea name="response" 
                                    type="text" 
                                    value={this.state.response} 
                                    onChange={this.handleComment} 
                                    placeholder="Add response"></textarea>
                                <button className="btn btn-primary" type="submit">Add response</button>
                                </form>
                            ) : (
                                <a onClick={(e) => this.toggleResponse(r, e)}>Respond</a>
                            )}
                        </div>
                        { this.renderResponses(r.responses) }

                    </li>
                    
                )}
            </ul>
        )
    }
    render(){
        return(
            <div className="wrapper">
            <div className="game">
                {!this.state.working ? (
                    <div>
                        <div className="game__details">
                            <h3>{this.state.data.name}</h3>
                            <p>{this.state.data.publisher}</p>
                        </div>
                        <h4>Comments</h4>
                        {this.state.data.comments.length ? (
                            <ul className="game__comments">
                            {this.state.data.comments.map(c => 
                                <li key={c.id} className="game__comments-item">
                                    <div className="game__comments-item-inner">
                                        <label><em>{c.user_name}</em></label>
                                        <span>{c.content}</span>
                                        {c.responding ? (
                                            <form className="game__comments-add" onSubmit={(e) => this.addResponse(c, e)}>
                                            <h5>Add response</h5>
                                            <textarea name="response" 
                                                type="text" 
                                                value={this.state.response} 
                                                onChange={this.handleComment} 
                                                placeholder="Add response"></textarea>
                                            <button className="btn btn-primary" type="submit">Add response</button>
                                            </form>
                                        ) : (
                                            <a onClick={(e) => this.toggleResponse(c, e)}>Respond</a>
                                        )}
                                    </div>   
                                    { this.renderResponses(c.responses) }
                                    
                                </li>
                            )}
                            </ul>
                        ) : (
                            <p>No comments..</p>
                        )}
                        <form className="game__comments-add" onSubmit={this.addComment}>
                            <h5>New comment</h5>
                        
                          <textarea name="comment" 
                                type="text" 
                                value={this.state.comment} 
                                onChange={this.handleComment} 
                                placeholder="Add comment"></textarea>
                             
                          <button className="btn btn-primary" type="submit">Add comment</button>
                        </form>
                    </div>
                ):(
                    <p>Working...</p>
                )}
            </div>
            </div>
        )
    }
}