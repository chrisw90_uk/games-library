import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

export class GamesView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            working: true,
            data: {}
        };
    }
    componentDidMount() {
      this.getGame()
    }
    getGame(){
      this.setState({working: true})
      axios.get('/api/game/' + this.props.match.params.id).then(res => {      
        this.setState({data: res.data, working: false})
      })
    }
    render(){
        return(
            <div className="games__view">
                    {!this.state.working ? (
                <h3>{this.state.data.name}</h3>) :
                    (<p>Working...</p>)}


            </div>
        )
    }
}