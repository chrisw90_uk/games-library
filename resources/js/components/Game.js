import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link } from "react-router-dom";

export class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
        };
    }
    render(){
        return(
            <div className="library__game">
                <Link to={"/dashboard/game/" + this.props.data.id}>
                    <h3 className="library__game-title">{this.props.data.name}</h3>
                    <p className="library__game-publisher">{this.props.data.publisher}</p>
                </Link>              
            </div> 
        )
    }
}