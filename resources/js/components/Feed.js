import React from 'react';
import ReactDOM from 'react-dom';

export class Feed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
        };
    }
    render(){
        return(
            <div className="wrapper">
                <div className="feed">
                    <h3>Feed</h3>
                    <p>This will be the feed of recent activity from other users</p>
                </div>
            </div>
        )
    }
}