import React from 'react';
import ReactDOM from 'react-dom';

export class Users extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
        };
    }
    render(){
        return(
            <div className="wrapper">
                <div className="users">
                    <h3>Users</h3>
                    <p>This will display a list of all the registered users.</p>
                    <p>It probably won't in the long run, but it will at least help with debugging.</p>
                </div>
            </div>
        )
    }
}