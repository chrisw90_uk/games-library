import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Game } from './Game';

export class GamesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          working: true,
          name: '',
          publisher: '',
          genre: 'moba',
          data: [],
          filteredData: [],
          filter: '',
          order: ''
        };
        this.addGame = this.addGame.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handleOrder = this.handleOrder.bind(this);
    }
    componentDidMount() {
      this.getGames()
    }
    getGames(){
      this.setState({working: true})
      axios.get('/api/games').then(res => {
          this.setState({data: res.data, filteredData: res.data, working: false})
      })
    }
    addGame(e) {
      e.preventDefault();
      this.setState({working: true})
      let game = {
        name: this.state.name,
        publisher: this.state.publisher,
        genre: this.state.genre
      }
      console.log(game)
      axios.post('/api/game', game).then(res => {
          console.log(res);
          this.setState({ name: '', publiser: ''})
          this.getGames();
      })
    }
    handleInputChange(e){
      this.setState({ [e.target.name]: e.target.value });
    }
    sortFunction(arr, x){
      return arr.sort((a,b) => {
          if(a[x] < b[x]) { return -1; }
          if(a[x] > b[x]) { return 1; }
          return 0;
        })
    }
    handleFilter(e){
      let order = this.state.order;
      if(e.target.value.length){
        let filtered = this.state.data.filter(function(game){
          return game.genre == e.target.value;
        })
        filtered = this.sortFunction(filtered, order);
        this.setState({ filteredData: filtered, filter: e.target.value });
      }else{
        let filtered = this.sortFunction(this.state.data, this.state.order);
        this.setState({ filteredData: filtered, filter: e.target.value });
      }
      
    }
    handleOrder(e){
      let sorted = this.sortFunction(this.state.filteredData, e.target.value);
      this.setState({ filteredData: sorted, order: e.target.value });
    }
    render(){

        let list;

        if(this.state.data.length){
            list = this.state.filteredData.map(game => 
              <Game key={game.name} data={game} />
            );
        }else{
            list = <p> Your library is empty </p>
        }

        return(
          <div>
          {!this.state.working ? (
            <div className="wrapper">
              {this.state.data.length > 2 ? (
            
              <div className="library__filter">
              <div className="library__filter-item">
                <label>Order by</label>
                <select name="sort" onChange={this.handleOrder}>
                    <option value="name">Name</option> 
                    <option value="publisher">Publisher</option> 
                </select>
              </div>
              <div className="library__filter-item">
                <label>Filter by genre</label>
                <select name="filter" onChange={this.handleFilter}>
                    <option value="">All</option> 
                    <option value="fps">FPS</option> 
                    <option value="rts">RTS</option>
                    <option value="mmo">MMO</option>
                    <option value="moba">MOBA</option>
                </select>
              </div>
              
              </div>
              ) : (<div></div>)}
              
              <div className="library__games">
                {list}
              </div>
              <div className="library__new">
              <h4>Add new game</h4>
                <form onSubmit={this.addGame}>
                <div>
                  <label>Name</label>
                  <input name="name" type="text" value={this.state.name} onChange={this.handleInputChange} />
                </div>
                <div>
                  <label>Publisher</label>
                  <input name="publisher" type="text" value={this.state.publisher} onChange={this.handleInputChange} />
                </div>
                <div>
                  <label>Genre</label>
                  <select name="genre" type="text" value={this.state.genre} onChange={this.handleInputChange}>
                    <option value="fps">FPS</option> 
                    <option value="rts">RTS</option>
                    <option value="mmo">MMO</option>
                    <option value="moba">MOBA</option>
                  </select>
                </div>
                <button className="btn btn-primary" type="submit">Add game</button>
                </form>
                </div>
              </div>
            ): (<p className="working">Working...</p>)}      
          </div>
        )
    }
}