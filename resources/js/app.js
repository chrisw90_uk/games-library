import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { GamesList } from './components/GamesList';
import { Feed } from './components/Feed';
import { GamesView } from './components/GamesView';
import { Users } from './components/Users';

const title = 'Games';

import '../sass/app.scss';



if(document.getElementById('app')){
	ReactDOM.render(
		<Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/dashboard">Home</Link>
            </li>
            <li>
              <Link to="/dashboard/feed/">Feed</Link>
            </li>
            <li>
              <Link to="/dashboard/users/">Users</Link>
            </li>
          </ul>
        </nav>

        <Route path="/dashboard" exact component={GamesList} />
        <Route path="/dashboard/feed/" component={Feed} />
        <Route path="/dashboard/users/" component={Users} />
        <Route path="/dashboard/game/:id" component={GamesView} />
      </div>
    </Router>,
		document.getElementById('app')
	);
}
